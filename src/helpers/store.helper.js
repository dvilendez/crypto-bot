import store from '../store/index'
import { useNamespacedState, useNamespacedGetters, useNamespacedMutations, useNamespacedActions } from 'vuex-composition-helpers'
const callAction = (type) => (namespace, elements) => {
  return type(
    store,
    namespace,
    elements
  )
}

const actions = {
  state: useNamespacedState,
  getters: useNamespacedGetters,
  mutations: useNamespacedMutations,
  actions: useNamespacedActions
}

export const useState = callAction(actions.state)
/**
 * @param {string} namespace
 * @param {string[]} elements
 *
 * @returns {[key: string]:{value: any}}
 */
export const useGetters = callAction(actions.getters)
export const useMutations = callAction(actions.mutations)
export const useActions = callAction(actions.state)

/**
 * @param {string} namespace
 * @param {{[key: string]: string[]}} data
 *
 * @returns {any}
 */
export const useStore = (namespace, data) => {
  const options = Object.keys(actions)
  let elements = {}
  for (const action in data) {
    const getElements = data[action]
    if (!options.includes(action)) {
      throw new Error(`The action ${action} is not valid`)
    }
    elements = {
      ...elements,
      ...callAction(actions[action])(namespace, getElements)
    }
  }
  return elements
}
