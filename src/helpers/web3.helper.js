// import factoryAbi from '@/abis/factory.abi'
// import pairAbi from '@/abis/pair.abi'
// import abis.token from '@/abis/token.abi'
// import routerAbi from '@/abis/router.abi'
import { abis } from '@/values/abis.value'
import Tx from 'ethereumjs-tx'

import { decimals } from '@/values/decimals.value'
// import { mainAddresses } from '@/values/mainAddresses.value'

export const getTokenProp = async (web3, tokenAddress, prop) => {
  tokenAddress = web3.utils.toChecksumAddress(tokenAddress)
  const contract = new web3.eth.Contract(abis.token, tokenAddress, (error, result) => { if (error) console.log(error) })
  return contract.methods[prop]().call()
}

export const balanceOf = async (web3, tokenAddress) => {
  let {
    VUE_APP_BOT_ADDRESS: botAddress
  } = process.env
  tokenAddress = web3.utils.toChecksumAddress(tokenAddress)
  botAddress = web3.utils.toChecksumAddress(botAddress)
  const contract = new web3.eth.Contract(abis.token, tokenAddress, (error, result) => { if (error) console.log(error) })
  return await contract.methods.balanceOf(botAddress).call()
}

export const getPairContract = async (web3, address0, address1, factoryAddress) => {
  const factoryContract = new web3.eth.Contract(abis.factory, factoryAddress, (error, result) => { if (error) console.log(error) })

  address0 = web3.utils.toChecksumAddress(address0)
  address1 = web3.utils.toChecksumAddress(address1)

  let pairAddress = await factoryContract.methods.getPair(address0, address1).call()
  // si no existe el contrato retornamos
  if (pairAddress === '0x0000000000000000000000000000000000000000') {
    return {
      state: false
    }
  }
  pairAddress = web3.utils.toChecksumAddress(pairAddress)
  const pairContract = new web3.eth.Contract(abis.pair, pairAddress, (error, result) => { if (error) console.log(error) })
  return {
    state: true,
    contract: pairContract
  }
}

export const getTokenPrice = async (
  web3,
  pairContract,
  mainTokenAddress,
  mainDecimals,
  secondaryDecimals
) => {
  let token0 = await pairContract.methods.token0().call()
  token0 = web3.utils.toChecksumAddress(token0)

  const reserves = await pairContract.methods.getReserves().call()

  const reserve0 = reserves[0]
  const reserve1 = reserves[1]
  if (reserve0 === '0' || reserve1 === '0') {
    return 0
  }

  let mainReserve = 0
  let secondaryReserve = 0
  if (mainTokenAddress === token0) {
    mainReserve = reserve1
    secondaryReserve = reserve0
  } else {
    mainReserve = reserve0
    secondaryReserve = reserve1
  }
  let tokenPrice = mainReserve / secondaryReserve
  // let tokenPrice = secondaryReserve / mainReserve
  tokenPrice = tokenPrice * Math.pow(10, mainDecimals - secondaryDecimals)

  return tokenPrice
}

export const getNonce = async (web3, address) => {
  let lastNonce = -1
  let nonce = await web3.eth.getTransactionCount(address)
  while (nonce <= lastNonce) {
    nonce = await web3.eth.getTransactionCount(address)
  }
  lastNonce = nonce
  return lastNonce
}

export const approve = async ({ web3, tokenAddress, gas, gwei, routerAddress, chainId }) => {
  try {
    tokenAddress = web3.utils.toChecksumAddress(tokenAddress)
    let {
      VUE_APP_BOT_ADDRESS: botAddress,
      VUE_APP_BOT_PRIVATE_KEY: privateKey
    } = process.env
    privateKey = Buffer.from(privateKey, 'hex')
    const tokenContract = new web3.eth.Contract(abis.token, tokenAddress, (error, result) => { if (error) console.log(error) })
    const nonce = await getNonce(web3, botAddress)

    const encoded = await tokenContract.methods.approve(
      routerAddress,
      // eslint-disable-next-line no-undef
      BigInt(11579208923731619542357098500868790785326998466564)
    ).encodeABI()

    const rawTx = {
      from: botAddress,
      to: tokenAddress,
      gasPrice: web3.utils.toHex(web3.utils.toWei(gwei, 'gwei')), // (30 / Math.pow(10, 18)).toString(),
      value: web3.utils.toHex(web3.utils.toWei('0', 'ether')), // '1000000', // web3Http.utils.toWei(amount, 'ether'),
      data: encoded,
      gas: gas,
      nonce: web3.utils.toHex(nonce),
      chainId
    }

    var tx = new Tx(rawTx)
    tx.sign(privateKey)

    const serializedTx = tx.serialize()

    await web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'))
    return 'APROBADO Y READY PARA VENDER!!!!!!!!!!!!'
  } catch (error) {
    return error
  }
}

export const swapExactETHForTokens = async ({ web3, tokenToBuy, tokenToSpend, amount, gas, gwei, routerAddress, blockchain, chainId }) => {
  try {
    let {
      VUE_APP_BOT_ADDRESS: botAddress,
      VUE_APP_BOT_PRIVATE_KEY: privateKey
    } = process.env
    privateKey = Buffer.from(privateKey, 'hex')
    tokenToBuy = web3.utils.toChecksumAddress(tokenToBuy)
    tokenToSpend = web3.utils.toChecksumAddress(tokenToSpend)
    const routerContract = new web3.eth.Contract(abis.router[blockchain], routerAddress, (error, result) => { if (error) console.log(error) })
    const nonce = await getNonce(web3, botAddress)

    const method = getMethodName(blockchain, 'swapExactETHForTokens', false)

    const encoded = await routerContract.methods[method](
      0,
      [tokenToSpend, tokenToBuy],
      botAddress,
      (new Date().getTime() + 100000)
    ).encodeABI()

    const rawTx = {
      from: botAddress,
      to: routerAddress,
      gasPrice: web3.utils.toHex(web3.utils.toWei(gwei, 'gwei')),
      value: web3.utils.toHex(web3.utils.toWei(amount, 'ether')),
      data: encoded,
      gas: gas,
      nonce: web3.utils.toHex(nonce),
      chainId
    }

    var tx = new Tx(rawTx)
    tx.sign(privateKey)

    const serializedTx = tx.serialize()

    await web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'))
    return 'Transacción completada exitosamente!!!!'
  } catch (error) {
    return error
  }
}

export const swapExactTokensForTokens = async ({ web3, tokenToBuy, tokenToSpend, amount, gas, gwei, decimalsTokenToSpend, routerAddress, supportingFee, blockchain, chainId }) => {
  try {
    let {
      VUE_APP_BOT_ADDRESS: botAddress,
      VUE_APP_BOT_PRIVATE_KEY: privateKey
    } = process.env
    privateKey = Buffer.from(privateKey, 'hex')
    tokenToBuy = web3.utils.toChecksumAddress(tokenToBuy)
    tokenToSpend = web3.utils.toChecksumAddress(tokenToSpend)
    const routerContract = new web3.eth.Contract(abis.router[blockchain], routerAddress, (error, result) => { if (error) console.log(error) })
    const nonce = await getNonce(web3, botAddress)

    let method = 'swapExactTokensForTokens'
    if (supportingFee) {
      method += 'SupportingFeeOnTransferTokens'
    }

    const encoded = await routerContract.methods[method](
      web3.utils.toWei(amount, decimals[decimalsTokenToSpend]),
      0,
      [tokenToSpend, tokenToBuy],
      botAddress,
      (new Date().getTime() + 100000)
    ).encodeABI()

    const rawTx = {
      from: botAddress,
      to: routerAddress,
      gasPrice: web3.utils.toHex(web3.utils.toWei(gwei, 'gwei')),
      value: web3.utils.toHex(web3.utils.toWei('0', 'ether')),
      data: encoded,
      gas: gas,
      nonce: web3.utils.toHex(nonce),
      chainId
    }

    var tx = new Tx(rawTx)
    tx.sign(privateKey)

    const serializedTx = tx.serialize()

    await web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'))
    return 'Transacción completada exitosamente!!!!'
  } catch (error) {
    return error
  }
}

export const swapExactTokensForETH = async ({ web3, tokenToBuy, tokenToSpend, amount, gas, gwei, decimalsTokenToSpend, routerAddress, supportingFee, blockchain, chainId }) => {
  try {
    let {
      VUE_APP_BOT_ADDRESS: botAddress,
      VUE_APP_BOT_PRIVATE_KEY: privateKey
    } = process.env
    privateKey = Buffer.from(privateKey, 'hex')
    tokenToBuy = web3.utils.toChecksumAddress(tokenToBuy)
    tokenToSpend = web3.utils.toChecksumAddress(tokenToSpend)
    const routerContract = new web3.eth.Contract(abis.router[blockchain], routerAddress, (error, result) => { if (error) console.log(error) })
    const nonce = await getNonce(web3, botAddress)
    const method = getMethodName(blockchain, 'swapExactTokensForETH', supportingFee)

    const encoded = await routerContract.methods[method](
      web3.utils.toWei(amount, decimals[decimalsTokenToSpend]),
      0,
      [tokenToSpend, tokenToBuy],
      botAddress,
      (new Date().getTime() + 100000)
    ).encodeABI()
    console.log(web3.utils.toString('0x2ecc889a00'))
    const rawTx = {
      from: botAddress,
      to: routerAddress,
      gasPrice: web3.utils.toHex(web3.utils.toWei(gwei, 'gwei')),
      // gasLimit,
      value: web3.utils.toHex(web3.utils.toWei('0', 'ether')),
      data: encoded,
      gas: parseInt(gas),
      nonce: web3.utils.toHex(nonce),
      chainId
    }
    var tx = new Tx(rawTx)
    tx.sign(privateKey)

    const serializedTx = tx.serialize()

    await web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'))
    return 'Transacción completada exitosamente!!!!'
  } catch (error) {
    return error
  }
}

export const swapTokensForExactTokens = async ({ web3, tokenToBuy, tokenToSpend, amount, amountToBuy, gas, gwei, decimalsTokenToSpend, decimalsTokenToBuy, routerAddress, supportingFee, blockchain, chainId }) => {
  try {
    let {
      VUE_APP_BOT_ADDRESS: botAddress,
      VUE_APP_BOT_PRIVATE_KEY: privateKey
    } = process.env
    privateKey = Buffer.from(privateKey, 'hex')
    tokenToBuy = web3.utils.toChecksumAddress(tokenToBuy)
    tokenToSpend = web3.utils.toChecksumAddress(tokenToSpend)
    const routerContract = new web3.eth.Contract(abis.router[blockchain], routerAddress, (error, result) => { if (error) console.log(error) })
    const nonce = await getNonce(web3, botAddress)
    const method = 'swapTokensForExactTokens'

    const encoded = await routerContract.methods[method](
      web3.utils.toWei(amountToBuy, decimals[decimalsTokenToBuy]),
      web3.utils.toWei(amount, decimals[decimalsTokenToSpend]),
      [tokenToSpend, tokenToBuy],
      botAddress,
      (new Date().getTime() + 100000)
    ).encodeABI()

    const rawTx = {
      from: botAddress,
      to: routerAddress,
      gasPrice: web3.utils.toHex(web3.utils.toWei(gwei, 'gwei')),
      // gasLimit,
      value: web3.utils.toHex(web3.utils.toWei('0', 'ether')),
      data: encoded,
      gas: parseInt(gas),
      nonce: web3.utils.toHex(nonce),
      chainId
    }

    var tx = new Tx(rawTx)
    tx.sign(privateKey)

    const serializedTx = tx.serialize()

    await web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'))
    return 'Transacción completada exitosamente!!!!'
  } catch (error) {
    return error
  }
}

export const getAddressWithPrivateKey = ({
  web3,
  privateKey
}) => {
  return web3.eth.accounts.privateKeyToAccount(privateKey).address
}

export const estimateGas = async ({
  web3,
  to,
  from,
  value,
  data
}) => {
  return await web3.eth.estimateGas({
    to,
    from,
    value: web3.utils.toHex(web3.utils.toWei(value.toString(), 'ether')),
    data
  })
}

export const signTransaction = async ({
  web3,
  txParams,
  privateKey
}) => {
  return await web3.eth.accounts.signTransaction(txParams, privateKey)
}

const getMethodName = (blockchain, baseMethod, supportingFee) => {
  let method = baseMethod
  if (blockchain === 'avalanche') {
    method = method.replace('ETH', 'AVAX')
  }
  if (supportingFee) {
    method += 'SupportingFeeOnTransferTokens'
  }
  return method
}

// export const action = async ({ web3, routerAddress, tokenToBuy, tokenToSpend, amount, gas, gwei, decimalsTokenToSpend, methodName, blockchain, args }) => {
//   try {
//     let {
//       VUE_APP_BOT_ADDRESS: botAddress,
//       VUE_APP_BOT_PRIVATE_KEY: privateKey
//     } = process.env
//     privateKey = Buffer.from(privateKey, 'hex')
//     tokenToBuy = web3.utils.toChecksumAddress(tokenToBuy)
//     tokenToSpend = web3.utils.toChecksumAddress(tokenToSpend)
//     const routerContract = new web3.eth.Contract(abis.router[blockchain], routerAddress, (error, result) => { if (error) console.log(error) })
//     const nonce = await getNonce(web3, botAddress)

//     // const encoded = await routerContract.methods[methodName](
//     //   web3.utils.toWei(amount, decimals[decimalsTokenToSpend]),
//     //   0,
//     //   [tokenToSpend, tokenToBuy],
//     //   botAddress,
//     //   (new Date().getTime() + 100000)
//     // ).encodeABI()

//     const encoded = await routerContract.methods[methodName](this, Array.prototype.slice.call(args, 1)).encodeABI()

//     let value = '0'

//     if (tokenToSpend === mainAddresses[blockchain].address) {
//       value = amount
//     }

//     const rawTx = {
//       from: botAddress,
//       to: routerAddress,
//       gasPrice: web3.utils.toHex(web3.utils.toWei(gwei, 'gwei')),
//       value: web3.utils.toHex(web3.utils.toWei(value, 'ether')),
//       data: encoded,
//       gas: gas,
//       nonce: web3.utils.toHex(nonce),
//       chainId: 43114
//     }

//     var tx = new Tx(rawTx)
//     tx.sign(privateKey)

//     const serializedTx = tx.serialize()

//     await web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'))
//     return 'Transacción completada exitosamente!!!!'
//   } catch (error) {
//     return error
//   }
// }
