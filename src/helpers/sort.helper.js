export const sortByDate = (data, type) => {
  data = data.sort((a, b) => {
    return new Date(a[type]).getTime() - new Date(b[type]).getTime()
  })
  return data
}

export const sortByInteger = (data, type) => {
  data = data.sort((a, b) => {
    return a[type] - b[type]
  })
  return data
}
