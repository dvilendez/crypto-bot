import dayjs from 'dayjs'
export const formatDateTime = (date, format = 'DD-MM-YYYY HH:mm:ss') => {
  return dayjs(date).format(format)
}

export const formatDate = (date, format = 'DD-MM-YYYY') => {
  return dayjs(date).format(format)
}

export const formatTime = (date, format = 'HH:mm:ss') => {
  return dayjs(date).format(format)
}
