import Vue from 'vue'

export const notify = (text, title, color) => {
  Vue.prototype.$vs.notification({
    duration: 6000,
    title,
    text,
    color
  })
}

export const successMessage = (text, title = 'Notificación') => {
  notify(text, title, 'success')
}

export const dangerMessage = (text = 'Un error inesperado ha ocurrido...', title = 'Lo sentimos') => {
  notify(text, title, 'danger')
}

export const warningMessage = (text, title = 'Notificación') => {
  notify(text, title, 'warn')
}

export const componentMessage = (content) => {
  Vue.prototype.$vs.notification({
    duration: 6000,
    flat: true,
    color: 'success',
    content
  })
}
