export const rpcs = {
  avalanche: {
    mainnet: 'https://api.avax.network/ext/bc/C/rpc',
    moralis: 'https://speedy-nodes-nyc.moralis.io/1381749d34d9e33e2edae1de/avalanche/mainnet',
    // wss: 'wss://api.avax.network/ext/bc/C/ws'
    wss: 'wss://speedy-nodes-nyc.moralis.io/1381749d34d9e33e2edae1de/avalanche/mainnet/ws'
  },
  bsc: {
    mainnet: 'https://bsc-dataseed1.ninicoin.io',
    // mainnet: 'https://bsc-dataseed.binance.org/',
    moralis: 'https://speedy-nodes-nyc.moralis.io/1381749d34d9e33e2edae1de/bsc/mainnet',
    // wss: 'wss://solitary-cool-pond.bsc.quiknode.pro/7f47474b14ccf41e4cc7332fc6a001575653e4d2/'
    wss: 'wss://speedy-nodes-nyc.moralis.io/1381749d34d9e33e2edae1de/bsc/mainnet/ws'
    // wss: 'wss://dark-lively-flower.bsc.quiknode.pro/9fbbdaaa9f37f9c701e4a6a65439088415ee31d9/'
  },
  fantom: {
    mainnet: 'https://rpcapi.fantom.network/',
    moralis: 'https://speedy-nodes-nyc.moralis.io/1381749d34d9e33e2edae1de/fantom/mainnet',
    wss: 'wss://api.avax.network/ext/bc/C/ws'
  },
  eth: {
    mainnet: 'https://speedy-nodes-nyc.moralis.io/1381749d34d9e33e2edae1de/eth/mainnet',
    moralis: 'https://speedy-nodes-nyc.moralis.io/1381749d34d9e33e2edae1de/eth/mainnet',
    wss: 'wss://api.avax.network/ext/bc/C/ws'
  }
}
