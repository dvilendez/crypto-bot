export const routers = {
  avalanche: {
    traderjoe: {
      name: 'Trader Joe',
      routerAddress: '0x60aE616a2155Ee3d9A68541Ba4544862310933d4',
      factoryAddress: '0x9Ad6C38BE94206cA50bb0d90783181662f0Cfa10'
    },
    pangolin: {
      name: 'Pangolin',
      routerAddress: '0xE54Ca86531e17Ef3616d22Ca28b0D458b6C89106',
      factoryAddress: '0xefa94DE7a4656D787667C749f7E1223D71E9FD88'
    }
    // bogswap: {
    //   name: 'Bogswap',
    //   routerAddress: '0xB099ED146fAD4d0dAA31E3810591FC0554aF62bB',
    //   factoryAddress: '0x9Ad6C38BE94206cA50bb0d90783181662f0Cfa10'
    // }
  },
  bsc: {
    pancake: {
      name: 'Pancake Swap',
      routerAddress: '0x10ED43C718714eb63d5aA57B78B54704E256024E',
      factoryAddress: '0xcA143Ce32Fe78f1f7019d7d551a6402fC5350c73'
    }
  },
  fantom: {
    spooky: {
      name: 'Spooky Swap',
      routerAddress: '0xF491e7B69E4244ad4002BC14e878a34207E38c29',
      factoryAddress: '0x152eE697f2E276fA89E96742e9bB9aB1F2E61bE3'
    },
    spirit: {
      name: 'Spirit Swap',
      routerAddress: '0x16327E3FbDaCA3bcF7E38F5Af2599D2DDc33aE52',
      factoryAddress: '0xEF45d134b73241eDa7703fa787148D9C9F4950b0'
    }
  },
  eth: {
    uni: {
      name: 'Uni Swap',
      routerAddress: '0x7a250d5630b4cf539739df2c5dacb4c659f2488d',
      factoryAddress: '0x5C69bEe701ef814a2B6a3EDD4B1652CB9cc5aA6f'
    }
  }
}
