import factoryAbi from '@/abis/factory.abi'
import pairAbi from '@/abis/pair.abi'
import tokenAbi from '@/abis/token.abi'
import avalancheRouterAbi from '@/abis/avalancheRouter.abi'
import bscRouterAbi from '@/abis/bscRouter.abi'
import fantomRouterAbi from '@/abis/fantomRouter.abi'
import ethRouterAbi from '@/abis/ethRouter.abi'

export const abis = {
  factory: factoryAbi,
  pair: pairAbi,
  token: tokenAbi,
  router: {
    avalanche: avalancheRouterAbi,
    bsc: bscRouterAbi,
    fantom: fantomRouterAbi,
    eth: ethRouterAbi
  }
}
