export const blockchains = {
  avalanche: {
    address: '0xB31f66AA3C1e785363F0875A1B74E27b85FD66c7', // AVAX
    stableAddress: '0xA7D7079b0FEaD91F3e65f86E8915Cb59c1a4C664', // USDC.e
    stableDecimals: 6,
    inverted: false,
    chainId: 43114
  },
  bsc: {
    address: '0xbb4CdB9CBd36B01bD1cBaEBF2De08d9173bc095c', // BNB
    stableAddress: '0xe9e7CEA3DedcA5984780Bafc599bD69ADd087D56', // BUSD
    stableDecimals: 18,
    inverted: true,
    chainId: 56
  },
  fantom: {
    address: '0x21be370D5312f44cB42ce377BC9b8a0cEF1A4C83', // FTM
    stableAddress: '0x04068DA6C83AFCFA0e13ba15A6696662335D5B75', // USDC
    stableDecimals: 6,
    inverted: true,
    chainId: 250
  },
  eth: {
    address: '0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2', // ETH
    stableAddress: '0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48', // USDC
    stableDecimals: 6,
    inverted: true,
    chainId: 1
  }
}
