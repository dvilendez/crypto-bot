import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
// import Pinksale from '../views/Pinksale.vue'
// import Gempad from '../views/Gempad.vue'
import '@/assets/scss/main.scss'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  }
  // {
  //   path: '/pinksale',
  //   name: 'Pinksale',
  //   component: Pinksale
  // },
  // {
  //   path: '/gempad',
  //   name: 'Gempad',
  //   component: Gempad
  // }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
