import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import VueCompositionAPI from '@vue/composition-api'
import Unicon from 'vue-unicons/dist/vue-unicons-vue2.umd'
import { uniPlus, uniTrashAlt, uniVolumeUp, uniVolumeMute } from 'vue-unicons/dist/icons'
import Vuesax from 'vuesax'
import 'vuesax/dist/vuesax.css'
import '@/assets/scss/main.scss'

Unicon.add([uniPlus, uniTrashAlt, uniVolumeUp, uniVolumeMute])

Vue.config.productionTip = false
Vue.use(VueCompositionAPI)
Vue.use(Vuesax)
Vue.use(Unicon)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
