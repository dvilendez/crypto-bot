import Vue from 'vue'
import Vuex from 'vuex'
import Global from './modules/global'
import Swaper from './modules/swaper'
Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    Global,
    Swaper
  }
})
