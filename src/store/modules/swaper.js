export default {
  namespaced: true,
  state: {
    token0: '',
    decimals0: 0,
    symbol0: '',
    token1: '',
    decimals1: 0,
    symbol1: '',
    token0Balance: '0',
    token0Price: 0,
    token0PriceInUsd: 0
  },
  mutations: {
    setToken0 (state, data) {
      state.token0 = data
    },
    setDecimals0 (state, data) {
      state.decimals0 = data
    },
    setSymbol0 (state, data) {
      state.symbol0 = data
    },
    setToken1 (state, data) {
      state.token1 = data
    },
    setDecimals1 (state, data) {
      state.decimals1 = data
    },
    setSymbol1 (state, data) {
      state.symbol1 = data
    },
    setToken0Balance (state, data) {
      state.token0Balance = data
    },
    setToken0Price (state, data) {
      state.token0Price = data
    },
    setToken0PriceInUsd (state, data) {
      state.token0PriceInUsd = data
    }
  },
  getters: {
    token0Usd (state) {
      return parseFloat(state.token0Balance) * state.token0PriceInUsd
    }
  }
}
