import { routers } from '@/values/routers.value'

export default {
  namespaced: true,
  state: {
    logs: [],
    web3: null,
    web3ForTxn: null,
    web3Wss: null,
    blockchain: 'bsc',
    router: 'pancake'
  },
  mutations: {
    pushLog (state, data) {
      state.logs.push(data)
      if (state.logs.length > 100) {
        state.logs.shift()
      }
    },
    clearLogs (state) {
      state.logs = []
    },
    setWeb3 (state, data) {
      state.web3 = data
    },
    setWeb3ForTxn (state, data) {
      state.web3ForTxn = data
    },
    setWeb3Wss (state, data) {
      state.web3Wss = data
    },
    setBlockchain (state, data) {
      state.blockchain = data
    },
    setRouter (state, data) {
      state.router = data
    }
  },
  getters: {
    invertedLog (state) {
      return state.logs.slice().reverse()
    },
    routerOptions (state) {
      const routerOptions = []
      Object.keys(routers[state.blockchain]).forEach(router => {
        routerOptions.push({
          name: routers[state.blockchain][router].name,
          value: router
        })
      })
      return routerOptions
    }
  }
}
